var gulp = require('gulp'),
    gulpLoadPlugins = require('gulp-load-plugins'),
    plugins = gulpLoadPlugins(),
	neat = require('node-neat').includePaths,
	gulpSequence = require('gulp-sequence');
	
var paths = {
		emails : {
			origin 	: "email_templates/**/*.html",
			dest	: "emails"
		},
		css : {
			origin 	: "dev/**/*.scss",
			dest	: "assets",
			path	: "dev/css"
		},
		images : {
			origin 	: "dev/images/**",
			dest	: "assets/images",
			path	: "dev/images"
		}
	}

// GULP TASKS	
// ==========================================================
	
	gulp.task('css', function(cb) {		
		var stream = gulp.src(paths.css.origin)
			.pipe(plugins.plumber())
			.pipe(plugins.sourcemaps.init())
			.pipe(plugins.sass({
				outputStyle: 'compressed',
				includePaths : [paths.css.path].concat(neat)
			}))
			.pipe(plugins.sourcemaps.write())
			.pipe(gulp.dest(paths.css.dest));

		stream.on('end',function() {
			cb();
		});
	});
	
	gulp.task('images', function(cb) {
		var stream = gulp.src(paths.images.path+"/*", { base: './dev/images' })
			.pipe(gulp.dest(paths.images.dest));


		stream.on('end',function() {
			cb();
		});

	});

	gulp.task('email', function() {
		return gulp.src(paths.emails.origin)
			.pipe(plugins.inlineCss({
				applyStyleTags: true,
				applyLinkTags: true,
				removeStyleTags: true,
				removeLinkTags: true
			}))
			.pipe(gulp.dest(paths.emails.dest))
			.pipe(plugins.livereload());
	});

	gulp.task('buildSeq', function(callback) {
		gulpSequence('css','images','email')(callback)
	});

// WATCH FILES
// ==========================================================
// run: gulp

	gulp.task('default', function () {
		plugins.livereload.listen();
		
		gulp.run('buildSeq');
		gulp.watch(paths.css.origin, ['buildSeq']);
		gulp.watch(paths.emails.origin, ['buildSeq']);

	});


// CREATE DISTRIBUTION FILES
// ==========================================================
// Run: gulp dist

	gulp.task('dist',function() {

		gulp.src(["**/*",
			"!.gitkeep",
			"!.gitignore",
			"!.gitmodules",
			"!composer.json",
			"!composer.lock",
			"!gulpfile.js",
			"!package.json",
			'!{dev,dev/**}',
			'!{build,build/**}',
			'!{node_modules,node_modules/**}',
			'!README.md',
			'!CommonCode.php'])
			.pipe(gulp.dest('build/'));
	});


// READ HOW MANY LINES IN A PROJECT THERE ARE
// ==========================================================
// Run: gulp lines

	gulp.task('lines',function() {
		gulp.src(["**/*.js",
			"**/*.php",
			"!.gitkeep",
			"!.gitignore",
			"!.gitmodules",
			"!composer.json",
			"!gulpfile.js",
			"!package.json",
			'!{assets,assets/**}',
			'!{build,build/**}',
			'!{node_modules,node_modules/**}',
			'!{vendor,vendor/**}',
			'!README.md',
			'!CommonCode.php'])
		.pipe(plugins.plumber())
    	.pipe(plugins.sloc());
	});




